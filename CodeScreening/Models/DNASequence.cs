﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodeScreening.Models
{

    /// <summary>
    /// Assumptions made:
    ///     1. The strings are small strings and can be handled in memory.
    ///     2. If there is a tie between two parents the first parent will be victorious.
    ///     3. The B was a mistake in the acceptance criteria / requirements             
    ///     4. If the user inputs the wrong child string (zero pattern matches)
    ///        it will return empty/null for the PrimaryDonorSequence and IntactSequence
    /// </summary>
    public class DNASequence

    {
        private string _parentSequence1;
        private string _parentSequence2;
        private string _sequence;
        private string _primaryDonorSequence;
        private string _intactSequence;

        #region Constructors


        public DNASequence(string parentSequence1, string parentSequence2, string sequence)
        {
            if (!ValidateSequence(parentSequence1))
                throw new ApplicationException("Parent 1 cannot have any characters except A, G, T, C");
            if (!ValidateSequence(parentSequence2))
                throw new ApplicationException("Parent 2 cannot have any characters except A, G, T, C");
            if (!ValidateSequence(sequence))
                throw new ApplicationException("Primary Sequence cannot have any characters except A, G, T, C");
            _sequence = sequence;
            _parentSequence1 = parentSequence1;
            _parentSequence2 = parentSequence2;
            SetPrimaryDonorAndIntactSequence();
        }

        #endregion

        #region Accessors

        public string PrimaryDonor
        {
            get
            {
                return _primaryDonorSequence;
            }
        }

        public string IntactSequence
        {
            get
            {
                return _intactSequence;
            }
        }

        #endregion

        #region Public Methods

        public static bool ValidateSequence(string sequence)
        {
            string regexPattern = "^[AGTC]*$";
            var regex = new Regex(regexPattern);
            return regex.IsMatch(sequence);
        }

        #endregion

        #region Private Methods

        private void SetPrimaryDonorAndIntactSequence()
        {
            string subPattern1 = "";
            string subPattern2 = "";
            int parentLength1 = CalculateLongestSubSequence(_parentSequence1, _sequence, out subPattern1);
            int parentLength2 = CalculateLongestSubSequence(_parentSequence2, _sequence, out subPattern2);
            
            if (parentLength1 >= parentLength2 && parentLength1 > 0)
            {
                _primaryDonorSequence = _parentSequence1;
                _intactSequence = subPattern1;
            }                
            else if (parentLength2 >= parentLength1 && parentLength2 > 0)
            {
                _primaryDonorSequence = _parentSequence2;
                _intactSequence = subPattern2;
            }
             
        }

        private int CalculateLongestSubSequenceNaive(string parentSequence, string childSequence, out string subSequence)
        {
            var parentSequenceCharArray = parentSequence.ToCharArray();
            var childSequenceCharArray = childSequence.ToCharArray();

            int maxLength = 0;
            string maxSubSequence = "";
            StringBuilder sbCurrentSubPattern = new StringBuilder();
            for(int i = 0; i < childSequenceCharArray.Length; i++)
            {
                for (int j = 0; j < parentSequenceCharArray.Length; j++)
                {
                    sbCurrentSubPattern.Clear();
                    int length = 0;
                    int m = i;
                    int n = j;
                    while (m < childSequenceCharArray.Length && n < parentSequenceCharArray.Length && childSequenceCharArray[m] == parentSequenceCharArray[n])
                    {
                        sbCurrentSubPattern.Append(childSequenceCharArray[m]);
                        ++length;
                        ++m;
                        ++n;
                    }
                    if (maxLength < length)
                    {
                        maxLength = length;
                        maxSubSequence = sbCurrentSubPattern.ToString();
                    }
                }
            }
            subSequence = maxSubSequence;
            return maxLength;

        }


        private int CalculateLongestSubSequence(string parentSequence, string childSequence, out string subSequence)
        {
            var parentSequenceCharArray = parentSequence.ToCharArray();
            var childSequenceCharArray = childSequence.ToCharArray();
            var matrix = new int[childSequence.Length, parentSequence.Length];
            
            int maxLength = 0;
            string maxSubSequence = "";
            StringBuilder sbCurrentSubPattern = new StringBuilder();
            for (int i = 0; i < childSequenceCharArray.Length; i++)
            {
                for (int j = 0; j < parentSequenceCharArray.Length; j++)
                {
                    sbCurrentSubPattern.Clear();
                    int length = 0;
                    if (i > 0 && j > 0)
                    {
                        length = matrix[i-1, j-1];
                        sbCurrentSubPattern.Append(childSequence.Substring(i-length, length));
                    }
                
                    if(childSequenceCharArray[i] == parentSequenceCharArray[j])
                    {
                        sbCurrentSubPattern.Append(childSequenceCharArray[i]);
                        ++length;
                        matrix[i, j] = length;
                    }

                    if (maxLength <= length)
                    {
                        maxLength = length;
                        maxSubSequence = sbCurrentSubPattern.ToString();
                    }
                }
            }
            subSequence = maxSubSequence;
            return maxLength;

        }

        #endregion




    }
}
