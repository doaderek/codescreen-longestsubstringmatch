﻿using CodeScreening.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeScreening
{
    class Program
    {
        static void Main(string[] args)
        {
            string exitString = "";
            while (exitString != "Q")
            {
                try
                {
                    Console.Write("Parent 1: ");
                    string parent1 = Console.ReadLine();
                    Console.Write("Parent 2: ");
                    string parent2 = Console.ReadLine();
                    Console.Write("Child: ");
                    string child = Console.ReadLine();
                    DNASequence sequence = new DNASequence(parent1, parent2, child);
                    Console.WriteLine("\nPrimary Donor: " + sequence.PrimaryDonor);
                    Console.WriteLine("\nIntact Sequence: " + sequence.IntactSequence);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Console.Write("\nPress any key to Start Over or Q to exit... ");
                exitString = Console.ReadLine();
            }


        }
    }
}
