﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CodeScreening.Models;

namespace CodeScreening.Test
{
    [TestClass]
    public class DNASequenceTests
    {
        [TestMethod]
        public void ValidateSequence_SequenceOnlyHasCharactersGATC_False()
        {
            bool x = DNASequence.ValidateSequence("CABACABATG");
            Assert.IsFalse(x);
        }

        [TestMethod]
        public void ValidateSequence_SequenceOnlyHasCharactersGATC_True()
        {
            Assert.IsTrue(DNASequence.ValidateSequence("GATTACA"));
        }

        [TestMethod]
        public void PrimaryDonorIntactSequence_FullSequenceParent1_ReturnsPatternFromParent1()
        {
            DNASequence sequence = new DNASequence("GATTACA","AAAAAAA","AAAGATTACAAAAA");
            Assert.AreEqual("GATTACA", sequence.IntactSequence);
            Assert.AreEqual("GATTACA", sequence.PrimaryDonor);
        }

        [TestMethod]
        public void PrimaryDonorIntactSequence_FullSequenceParent2_ReturnsPatternFromParent2()
        {
            DNASequence sequence = new DNASequence("AAAAAAA", "GATTACA", "AAAGATTACAAAAA");
            Assert.AreEqual("GATTACA", sequence.IntactSequence);
            Assert.AreEqual("GATTACA", sequence.PrimaryDonor);
        }

        [TestMethod]
        public void PrimaryDonorIntactSequence_SubSequenceParent1_ReturnsPatternFromParent1()
        {
            DNASequence sequence = new DNASequence("GATTACA", "AAAGAAA", "AGAAGATTAAAACAA");
            Assert.AreEqual("GATTA", sequence.IntactSequence);
            Assert.AreEqual("GATTACA", sequence.PrimaryDonor);
        }

        [TestMethod]
        public void PrimaryDonorIntactSequence_SubSequenceParent2_ReturnsPatternFromParent2()
        {
            DNASequence sequence = new DNASequence("AAAGAAA", "GATTACA", "AGAAGATTAAAACAA");
            Assert.AreEqual("GATTA", sequence.IntactSequence);
            Assert.AreEqual("GATTACA", sequence.PrimaryDonor);
        }

        [TestMethod]
        public void PrimaryDonorIntactSequence_EqualSubSequence_ReturnsPatternFromParent1()
        {
            DNASequence sequence = new DNASequence("AAAAAAA", "GATTACA", "AAAGATTAAAAACAA");
            Assert.AreEqual("AAAAA", sequence.IntactSequence);
            Assert.AreEqual("AAAAAAA", sequence.PrimaryDonor);
        }

        [TestMethod]
        public void PrimaryDonorIntactSequence_OneCharacter_ReturnsPatternFromParent1()
        {
            DNASequence sequence = new DNASequence("A", "G", "AG");
            Assert.AreEqual("A", sequence.IntactSequence);
            Assert.AreEqual("A", sequence.PrimaryDonor);
        }

        [TestMethod]
        public void PrimaryDonorIntactSequence_LargeSequence_ReturnsPatternFromParent1()
        {
            DNASequence sequence = new DNASequence(
                "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 
                "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", 
                "AGAGAAAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAAAGAGAGAGAGGGGGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAG"+
                "AGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAGAG");
            Assert.AreEqual("GGGGG", sequence.IntactSequence);
            Assert.AreEqual("GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG", sequence.PrimaryDonor);
        }

        [TestMethod]
        public void PrimaryDonorIntactSequence_WrongChild_ReturnsEmptyStrings()
        {
            DNASequence sequence = new DNASequence("A", "G", "C");
            Assert.IsTrue(String.IsNullOrEmpty(sequence.IntactSequence));
            Assert.IsTrue(String.IsNullOrEmpty(sequence.PrimaryDonor));
        }
    }
}
